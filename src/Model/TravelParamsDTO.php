<?php

namespace RL\Model;

class TravelParamsDTO
{
    /** @var float */
    protected $latitudeFrom;
    /** @var float */
    protected $longitudeFrom;
    /** @var float */
    protected $latitudeTo;
    /** @var float */
    protected $longitudeTo;
    /**
     * Speed represented in km/h
     *
     * @var null|float
     */
    protected $speed;

    /**
     * @return float
     */
    public function getLatitudeFrom(): float
    {
        return $this->latitudeFrom;
    }

    /**
     * @param float $latitudeFrom
     */
    public function setLatitudeFrom(float $latitudeFrom): self
    {
        $this->latitudeFrom = $latitudeFrom;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitudeFrom(): float
    {
        return $this->longitudeFrom;
    }

    /**
     * @param float $longitudeFrom
     */
    public function setLongitudeFrom(float $longitudeFrom): self
    {
        $this->longitudeFrom = $longitudeFrom;

        return $this;
    }

    /**
     * @return float
     */
    public function getLatitudeTo(): float
    {
        return $this->latitudeTo;
    }

    /**
     * @param float $latitudeTo
     */
    public function setLatitudeTo(float $latitudeTo): self
    {
        $this->latitudeTo = $latitudeTo;

        return $this;
    }

    /**
     * @return float
     */
    public function getLongitudeTo(): float
    {
        return $this->longitudeTo;
    }

    /**
     * @param float $longitudeTo
     */
    public function setLongitudeTo(float $longitudeTo): self
    {
        $this->longitudeTo = $longitudeTo;

        return $this;
    }

    /**
     * @return float
     */
    public function getSpeed(): ?float
    {
        return $this->speed;
    }

    /**
     * @param float $speed
     */
    public function setSpeed(?float $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

}