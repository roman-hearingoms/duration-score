<?php

namespace RL;

use RL\Model\TravelParamsDTO;
use RL\Strategy\TimeEstimationStrategyInterface;
use RL\Transformer\DurationScoreTransformerInterface;

class DurationScoreService implements DurationScoreInterface
{
    /** @var TimeEstimationStrategyInterface */
    private $durationStrategy;
    /** @var DurationScoreTransformerInterface */
    private $scoreTransformer;

    public function __construct(
        TimeEstimationStrategyInterface $strategy,
        DurationScoreTransformerInterface $transformer
    ) {
        $this->durationStrategy = $strategy;
        $this->scoreTransformer = $transformer;
    }

    public function getScore(TravelParamsDTO $params): int
    {
        $travelDuration = $this->durationStrategy->estimate($params);

        return $this->scoreTransformer->transform($travelDuration);
    }
}