<?php

namespace RL\Traits;

trait ClassConstantsTrait
{
    /**
     * Gets named constants.
     *
     * @param string $name
     *
     * @return array
     */
    public static function namedConstants($name)
    {
        $reflection = new \ReflectionClass(get_called_class());
        $constants = $reflection->getConstants();

        $keys = array_filter(
            array_keys($constants),
            function ($value) use ($name) {
                return preg_match('/^'.$name.'_/', $value);
            }
        );

        return array_intersect_key($constants, array_flip($keys));
    }
}
