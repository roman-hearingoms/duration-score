<?php

namespace RL\Exceptions;

/**
 * Class MapHandlingException
 *
 * Unable to perform or parse external map service request
 */
class MapHandlingException extends \Exception {
}