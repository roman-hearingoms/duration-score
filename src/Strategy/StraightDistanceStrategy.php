<?php

namespace RL\Strategy;

use RL\Model\TravelParamsDTO;

class StraightDistanceStrategy implements TimeEstimationStrategyInterface
{
    const EARTH_RADIUS_KM = 6371;

    function estimate(TravelParamsDTO $params): int
    {
        $distance = $this->getStraightDistance($params);

        $timeHours = $distance / $params->getSpeed();
        $timeMinutes = (int) round ($timeHours * 60);

        return $timeMinutes;
    }

    /**
     * Calculate distance between 2 points
     * based on Vincenty formula
     *
     * @param TravelParamsDTO $params
     * @return float - kilometers
     */
    protected function getStraightDistance(TravelParamsDTO $params): float
    {
        $latFrom = deg2rad($params->getLatitudeFrom());
        $lonFrom = deg2rad($params->getLongitudeFrom());
        $latTo = deg2rad($params->getLatitudeTo());
        $lonTo = deg2rad($params->getLongitudeTo());

        $lonDelta = $lonTo - $lonFrom;

        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        return $angle * self::EARTH_RADIUS_KM;
    }
}