<?php

namespace RL\Strategy;

use RL\Driver\MapResourceInteractionFacade;
use RL\Exceptions\MapHandlingException;
use RL\Model\TravelParamsDTO;

class GoogleDirectionsStrategy implements TimeEstimationStrategyInterface
{
    /** @var MapResourceInteractionFacade */
    private $facade;

    public function __construct(MapResourceInteractionFacade $facade)
    {
        $this->facade = $facade;
    }

    public function estimate(TravelParamsDTO $params): int
    {
        try {
            $response = $this->facade->doRequest($params);
            if (!$response) {
                throw new \RuntimeException('Unable to perform or handle request!');
            }

            if ($speed = $params->getSpeed()) {
                return $this->customSpeedScenario($response, $speed);
            }

            return $this->googleEstimatedSpeedScenario($response);
        } catch (\Throwable $exception) {
            // TODO:: implement a better error handling
            throw new MapHandlingException($exception->getMessage());
        }
    }

    protected function customSpeedScenario(\stdClass $response, float $speed): int
    {
        $distanceMeters = $response->routes[0]->legs[0]->distance->value;
        $distanceKilometers = $distanceMeters / 1000;

        $timeHours = $distanceKilometers / $speed;
        $timeMinutes = (int) round($timeHours * 60);

        return $timeMinutes;
    }

    protected function googleEstimatedSpeedScenario(\stdClass $response): int
    {
        $durationSeconds = $response->routes[0]->legs[0]->duration->value;
        $durationMinutes = floor($durationSeconds / 60);

        return $durationMinutes;
    }
}
