<?php

namespace RL\Strategy;

use RL\Exceptions\MapHandlingException;
use RL\Model\TravelParamsDTO;

interface TimeEstimationStrategyInterface
{
    /**
     * Estimate travel duration.
     *
     * @throws MapHandlingException
     *
     * @param TravelParamsDTO $params
     * @return int - minutes
     */
    function estimate(TravelParamsDTO $params): int;
}