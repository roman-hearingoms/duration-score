<?php

namespace RL\Driver;

class GoogleMapResponseJSONParser implements GoogleMapResponseParserInterface
{
    public function parse(string $contentBody): ?\stdClass
    {
        return json_decode($contentBody);
    }
}
