<?php

namespace RL\Driver;

use RL\Model\TravelParamsDTO;
use RuntimeException;
use stdClass;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;

class MapResourceInteractionFacade
{
    /** @var HttpClientInterface */
    private $httpClient;
    /** @var MapQueryBuilderInterface */
    private $builder;
    /** @var GoogleMapResponseParserInterface */
    private $parser;

    public function __construct(
        HttpClientInterface $httpClient,
        MapQueryBuilderInterface $builder,
        GoogleMapResponseParserInterface $parser
    ) {
        $this->httpClient = $httpClient;
        $this->builder = $builder;
        $this->parser = $parser;
    }

    public function doRequest(TravelParamsDTO $paramsDTO): ?stdClass
    {
        try {
            $queryString = $this->buildQuery($paramsDTO)->getUrl();

            $response = $this->httpClient->request('GET', $queryString);

            $content = $response->getContent();

            return $this->parser->parse($content);
        } catch (Throwable $exception) {
            throw new RuntimeException($exception->getMessage());
        }
    }

    private function buildQuery(TravelParamsDTO $params): MapQueryBuilderInterface
    {
        return $this->builder
            ->setOrigin(
                $this->prepareCoord($params->getLatitudeFrom()),
                $this->prepareCoord($params->getLongitudeFrom())
            )
            ->setDestination(
                $this->prepareCoord($params->getLatitudeTo()),
                $this->prepareCoord($params->getLongitudeTo())
            )
            ->setKey($this->getApiKey());
    }

    private function getApiKey(): string
    {
        // TODO:: add separate config file for testing env
        $config = include __DIR__.'/../../config.php';

        return $config['google_api_key'];
    }

    private function prepareCoord(float $float): string
    {
        return number_format($float, 5, '.', '');
    }
}
