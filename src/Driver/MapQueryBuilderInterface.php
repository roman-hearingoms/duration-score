<?php

namespace RL\Driver;

interface MapQueryBuilderInterface
{
    /**
     * Set origin coordinates.
     */
    function setOrigin(string $latitude, string $longitude): MapQueryBuilderInterface;

    /**
     * Set destination coordinates.
     */
    function setDestination(string $latitude, string $longitude): MapQueryBuilderInterface;

    /**
     * Set response format.
     *
     * @throws \InvalidArgumentException
     */
    function setOutputFormat(string $format): MapQueryBuilderInterface;

    /**
     * Set travel mode.
     *
     * @throws \InvalidArgumentException
     */
    function setMode(string $mode): MapQueryBuilderInterface;

    /**
     * Set security key.
     */
    function setKey(string $key): MapQueryBuilderInterface;

    /**
     * Build url for request.
     *
     * @throws \RuntimeException
     */
    function getUrl(): string;
}