<?php

namespace RL\Driver;

interface GoogleMapResponseParserInterface
{
    function parse(string $contentBody): ?\stdClass;
}