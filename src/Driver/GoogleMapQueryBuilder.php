<?php

namespace RL\Driver;

use InvalidArgumentException;
use RL\Traits\ClassConstantsTrait;
use RuntimeException;

/**
 * Class GoogleMapQueryBuilder.
 *
 * @see https://developers.google.com/maps/documentation/directions/intro
 */
class GoogleMapQueryBuilder implements MapQueryBuilderInterface
{
    use ClassConstantsTrait;

    const BASE_MAPS_URL = 'https://maps.googleapis.com/maps/api/directions';

    const OUTPUT_FORMAT_JSON = 'json';
    const OUTPUT_FORMAT_XML = 'xml';

    const TRAVEL_MODE_DRIVING = 'driving';
    const TRAVEL_MODE_WALKING = 'walking';
    const TRAVEL_MODE_BICYCLING = 'bicycling';
    const TRAVEL_MODE_TRANSIT = 'transit';

    /** @var string */
    protected $outputFormat;
    /** @var array */
    protected $origin;
    /** @var array */
    protected $destination;
    /** @var string */
    protected $key;
    /** @var string */
    protected $mode;

    public function getUrl(): string
    {
        $queryParams = [
            $this->getKey(),
            $this->getOrigin(),
            $this->getDestination(),
            $this->getMode(),
        ];

        $queryParamsString = implode('&', $queryParams);

        return sprintf('%s/%s?%s', self::BASE_MAPS_URL, $this->getOutputFormat(), $queryParamsString);
    }

    public function setOrigin(string $latitude, string $longitude): MapQueryBuilderInterface
    {
        $this->origin = [$latitude, $longitude];

        return $this;
    }

    public function setDestination(string $latitude, string $longitude): MapQueryBuilderInterface
    {
        $this->destination = [$latitude, $longitude];

        return $this;
    }

    public function setOutputFormat(string $format): MapQueryBuilderInterface
    {
        if (!in_array($format, array_values(self::namedConstants('OUTPUT_FORMAT')))) {
            throw new InvalidArgumentException(sprintf('Invalid output format: %s', $format));
        }

        $this->outputFormat = $format;

        return $this;
    }

    public function setKey(string $key): MapQueryBuilderInterface
    {
        $this->key = $key;

        return $this;
    }

    public function setMode(string $mode): MapQueryBuilderInterface
    {
        if (!in_array($mode, array_values(self::namedConstants('TRAVEL_MODE')))) {
            throw new InvalidArgumentException(sprintf('Invalid travel mode: %s', $mode));
        }

        $this->mode = $mode;

        return $this;
    }

    protected function getOrigin(): string
    {
        if (!is_array($this->origin)) {
            throw new RuntimeException('Origin is not set!');
        }

        return sprintf('origin=%s,%s', $this->origin[0], $this->origin[1]);
    }

    protected function getDestination(): string
    {
        if (!is_array($this->destination)) {
            throw new RuntimeException('Destination is not set!');
        }

        return sprintf('destination=%s,%s', $this->destination[0], $this->destination[1]);
    }

    protected function getKey(): string
    {
        if (!$this->key) {
            throw new RuntimeException('Google API key is required!');
        }

        return sprintf('key=%s', $this->key);
    }

    protected function getMode(): string
    {
        if (!$this->mode) {
            $this->mode = self::TRAVEL_MODE_DRIVING;
        }

        return sprintf('mode=%s', $this->mode);
    }

    protected function getOutputFormat(): string
    {
        if (!$this->outputFormat) {
            $this->outputFormat = self::OUTPUT_FORMAT_JSON;
        }

        return $this->outputFormat;
    }
}
