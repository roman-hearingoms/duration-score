<?php

namespace RL;

use RL\Exceptions\MapHandlingException;
use RL\Model\TravelParamsDTO;

interface DurationScoreInterface
{
    /**
     * Represent travel as score.
     *
     * @throws MapHandlingException
     */
    function getScore(TravelParamsDTO $params): int;
}