<?php

namespace RL\Transformer;

class LogarithmTransformer implements DurationScoreTransformerInterface
{
    public function transform(int $duration): int
    {
        return (int) round(log($duration, 2));
    }
}