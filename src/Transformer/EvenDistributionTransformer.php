<?php

namespace RL\Transformer;

class EvenDistributionTransformer implements DurationScoreTransformerInterface
{
    public function transform(int $duration): int
    {
        if ($duration < 0) {
            return 0;
        }

        if ($duration > 30) {
            return 100;
        }

        return $this->getPoints($duration);
    }

    protected function getPoints(int $duration): int
    {
        return round(($duration / 30) * 100);
    }
}