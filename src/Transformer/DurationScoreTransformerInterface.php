<?php

namespace RL\Transformer;

interface DurationScoreTransformerInterface
{
    /**
     * Transform travel duration, represented in minutes
     * into score points
     *
     * @param int $duration
     * @return int
     */
    public function transform(int $duration): int;
}