<?php

namespace RL\Transformer;

class PlainTransformer implements DurationScoreTransformerInterface
{
    public function transform(int $duration): int
    {
        if ($duration < 0) {
            return 0;
        }

        return $duration;
    }
}