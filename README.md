**Requirements:**

* PHP ^7.2
* Composer

**Installation**

Run `composer install`

**Tests**

Run `composer tests`

**Usage example**

    new DurationScoreService(
        new GoogleDirectionsStrategy(
            new MapResourceInteractionFacade(
                HttpClient::create(),
                new GoogleMapQueryBuilder(),
                new GoogleMapResponseJSONParser()
            )
        ),
        new EvenDistributionTransformer()
    );
