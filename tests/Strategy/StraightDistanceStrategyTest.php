<?php

namespace RL\Tests\Strategy;

use PHPUnit\Framework\TestCase;
use RL\Model\TravelParamsDTO;
use RL\Strategy\StraightDistanceStrategy;

final class StraightDistanceStrategyTest extends TestCase
{
    /** @var StraightDistanceStrategy */
    protected $strategy;

    protected function setUp(): void
    {
        $this->strategy = new StraightDistanceStrategy();
    }

    public function testDurationBetweenSamePointsMustBeZero(): void
    {
        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(33)
            ->setLongitudeFrom(33)
            ->setLatitudeTo(33)
            ->setLongitudeTo(33)
            ->setSpeed(30);

        static::assertEquals(0, $this->strategy->estimate($params));
    }

    public function testDurationBetweenDifferentPoints(): void
    {
        $params = (new TravelParamsDTO())
            // Berlin
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            // Munich
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58)
            ->setSpeed(30);

        // Berlin -> Munich = ~505 km, 505 / 30 = 16.8h = ~1010 minutes
        static::assertEquals(1010, $this->strategy->estimate($params));
    }
}