<?php

namespace RL\Tests\Strategy;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RL\Driver\MapResourceInteractionFacade;
use RL\Exceptions\MapHandlingException;
use RL\Model\TravelParamsDTO;
use RL\Strategy\GoogleDirectionsStrategy;

final class GoogleDirectionsStrategyTest extends TestCase
{
    /** @var MockObject|MapResourceInteractionFacade */
    private $facadeMock;

    public function setUp(): void
    {
        $this->facadeMock = $this->getMockBuilder(MapResourceInteractionFacade::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    public function testCustomSpeedScenario(): void
    {
        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58)
            ->setSpeed(30);

        $strategy = $this->buildService();
        $this->facadeMock->expects($this->once())->method('doRequest')
            ->with($params)
            ->willReturn(json_decode($this->getTestBody()));

        $time = $strategy->estimate($params);
        static::assertEquals($time, 1169);
    }

    public function testGoogleEstimatedSpeedScenario(): void
    {
        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58);

        $strategy = $this->buildService();
        $this->facadeMock->expects($this->once())->method('doRequest')
            ->with($params)
            ->willReturn(json_decode($this->getTestBody()));

        $time = $strategy->estimate($params);
        static::assertEquals($time, 353);
    }

    public function testBrokenResponseThrowsException(): void
    {
        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58)
            ->setSpeed(30);

        $strategy = $this->buildService();
        $this->facadeMock->expects($this->once())->method('doRequest')
            ->with($params)
            ->willReturn(new \stdClass());

        static::expectException(MapHandlingException::class);
        $strategy->estimate($params);
    }

    public function testEmptyResponseThrowsException(): void
    {
        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58)
            ->setSpeed(30);

        $strategy = $this->buildService();
        $this->facadeMock->expects($this->once())->method('doRequest')
            ->with($params)
            ->willReturn(null);

        static::expectException(MapHandlingException::class);
        static::expectExceptionMessage('Unable to perform or handle request!');
        $strategy->estimate($params);
    }

    private function buildService(): GoogleDirectionsStrategy
    {
        return new GoogleDirectionsStrategy($this->facadeMock);
    }

    private function getTestBody(): string
    {
        return file_get_contents(__DIR__.'/../Resources/test-google-map-response.json');
    }
}
