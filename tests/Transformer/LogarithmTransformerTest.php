<?php

namespace RL\Tests\Transformer;

use PHPUnit\Framework\TestCase;
use RL\Transformer\LogarithmTransformer;

final class LogarithmTransformerTest extends TestCase
{
    /**
     * @dataProvider logarithmTransformerDataProvider
     *
     * @param int $inputDuration
     * @param int $outputScore
     */
    public function testLogarithmTransformer(int $inputDuration, int $outputScore): void
    {
        $transformer = new LogarithmTransformer();

        static::assertEquals($outputScore, $transformer->transform($inputDuration));
    }

    public function logarithmTransformerDataProvider(): array
    {
        return [
            [-999, 0],
            [0, 0],
            [1, 0],
            [2, 1],
            [3, 2],
        ];
    }
}