<?php

namespace RL\Tests\Transformer;

use PHPUnit\Framework\TestCase;
use RL\Transformer\PlainTransformer;

final class PlainTransformerTest extends TestCase
{
    public function testTransformMethodReturnsWhatItGets(): void
    {
        $transformer = new PlainTransformer();

        self::assertEquals(0, $transformer->transform(-999));
        self::assertEquals(0, $transformer->transform(0));
        self::assertEquals(10, $transformer->transform(10));
    }
}