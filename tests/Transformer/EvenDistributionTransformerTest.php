<?php

namespace RL\Tests\Transformer;

use PHPUnit\Framework\TestCase;
use RL\Transformer\EvenDistributionTransformer;

final class EvenDistributionTransformerTest extends TestCase
{
    /**
     * @dataProvider evenDistributionDataProvider
     *
     * @param int $inputDuration
     * @param int $outputScore
     */
    public function testEvenDistributionTransformer(int $inputDuration, int $outputScore): void
    {
        $transformer = new EvenDistributionTransformer();

        static::assertEquals($outputScore, $transformer->transform($inputDuration));
    }

    public function evenDistributionDataProvider(): array
    {
        return [
            [-999, 0],
            [0, 0],
            [10, 33],
            [15, 50],
            [30, 100],
            [31, 100],
            [999, 100],
        ];
    }
}