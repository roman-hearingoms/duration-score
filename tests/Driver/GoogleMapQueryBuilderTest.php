<?php

namespace RL\Tests\Driver;

use PHPUnit\Framework\TestCase;
use RL\Driver\GoogleMapQueryBuilder;

class GoogleMapQueryBuilderTest extends TestCase
{
    public function testRegularBuilderFlow(): void
    {
        $qb = new GoogleMapQueryBuilder();

        $qb
            ->setOrigin('1.22', '2.33')
            ->setDestination('3.44', '5.66')
            ->setKey('abcKey');

        static::assertEquals(
            sprintf('%s/json?key=abcKey&origin=1.22,2.33&destination=3.44,5.66&mode=driving', GoogleMapQueryBuilder::BASE_MAPS_URL),
            $qb->getUrl()
        );

        $qb
            ->setMode(GoogleMapQueryBuilder::TRAVEL_MODE_BICYCLING)
            ->setOutputFormat(GoogleMapQueryBuilder::OUTPUT_FORMAT_XML);

        static::assertEquals(
            sprintf('%s/xml?key=abcKey&origin=1.22,2.33&destination=3.44,5.66&mode=bicycling', GoogleMapQueryBuilder::BASE_MAPS_URL),
            $qb->getUrl()
        );
    }

    public function testInvalidOutputFormat(): void
    {
        $qb = new GoogleMapQueryBuilder();

        static::expectException(\InvalidArgumentException::class);
        static::expectExceptionMessage('Invalid output format: invalid format');

        $qb->setOutputFormat('invalid format');
    }

    public function testInvalidTravelMode(): void
    {
        $qb = new GoogleMapQueryBuilder();

        static::expectException(\InvalidArgumentException::class);
        static::expectExceptionMessage('Invalid travel mode: invalid mode');

        $qb->setMode('invalid mode');
    }

    /**
     * @dataProvider missingParamsDataProvider
     */
    public function testMissingParamsDuringBuild(string $exceptionClass, string $exceptionMessage, GoogleMapQueryBuilder $qb): void
    {
        static::expectException($exceptionClass);
        static::expectExceptionMessage($exceptionMessage);

        $qb->getUrl();
    }

    public function missingParamsDataProvider(): array
    {
        $qbNoOrigin = (new GoogleMapQueryBuilder())
            ->setDestination('1', '2')
            ->setKey('asd');

        $qbNoDestination = (new GoogleMapQueryBuilder())
            ->setOrigin('1', '2')
            ->setKey('asd');

        $qbNoKey = (new GoogleMapQueryBuilder())
            ->setOrigin('1', '2')
            ->setDestination('1', '2');

        return [
            [\RuntimeException::class, 'Origin is not set!', $qbNoOrigin],
            [\RuntimeException::class, 'Destination is not set!', $qbNoDestination],
            [\RuntimeException::class, 'Google API key is required!', $qbNoKey],
        ];
    }
}
