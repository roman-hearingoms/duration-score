<?php

namespace RL\Tests\Driver;

use PHPUnit\Framework\TestCase;
use RL\Driver\GoogleMapResponseJSONParser;

class GoogleMapResponseJSONParserTest extends TestCase
{
    public function testCorrectJSONParse(): void
    {
        $json = '{"a": 123}';

        $parser = new GoogleMapResponseJSONParser();
        $result = $parser->parse($json);

        static::assertEquals(123, $result->a);
    }

    public function testIncorrectJSONParse(): void
    {
        $json = '"a": 123';

        $parser = new GoogleMapResponseJSONParser();
        $result = $parser->parse($json);

        static::assertEquals(null, $result);
    }
}
