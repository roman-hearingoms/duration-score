<?php

namespace RL\Tests\Driver;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RL\Driver\GoogleMapResponseParserInterface;
use RL\Driver\MapQueryBuilderInterface;
use RL\Driver\MapResourceInteractionFacade;
use RL\Model\TravelParamsDTO;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class MapResourceInteractionFacadeTest extends TestCase
{
    /** @var MockObject|HttpClientInterface */
    protected $httpClient;
    /** @var MockObject|MapQueryBuilderInterface */
    protected $builder;
    /** @var MockObject|GoogleMapResponseParserInterface */
    protected $parser;

    public function setUp(): void
    {
        $this->httpClient = $this->getMockBuilder(HttpClientInterface::class)->getMock();
        $this->builder = $this->getMockBuilder(MapQueryBuilderInterface::class)->getMock();
        $this->parser = $this->getMockBuilder(GoogleMapResponseParserInterface::class)->getMock();
    }

    public function testSuccessfulRequestCase(): void
    {
        $facade = $this->buildService();

        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58);

        $testUrl = 'test-url';
        $testJSONResponse = '{"a": 999}';

        $testStd = new \stdClass();
        $testStd->a = 999;

        $this->builder->expects($this->once())->method('setOrigin')
            ->with('52.52000', '13.40000')
            ->willReturn($this->builder);

        $this->builder->expects($this->once())->method('setDestination')
            ->with('48.13000', '11.58000')
            ->willReturn($this->builder);

        $this->builder->expects($this->once())->method('setKey')
            ->with($this->isType('string'))
            ->willReturn($this->builder);

        $this->builder->expects($this->once())->method('getUrl')
            ->willReturn($testUrl);

        $response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $response->expects($this->once())->method('getContent')
            ->willReturn($testJSONResponse);

        $this->httpClient->expects($this->once())->method('request')
            ->with('GET', $testUrl)
            ->willReturn($response);

        $this->parser->expects($this->once())->method('parse')
            ->with($testJSONResponse)
            ->willReturn($testStd);

        static::assertEquals($testStd, $facade->doRequest($params));
    }

    public function testFailedRequestCase(): void
    {
        $facade = $this->buildService();

        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58);

        $this->httpClient->expects($this->once())->method('request')
            ->willThrowException(new \InvalidArgumentException('test-message'));

        static::expectException(\RuntimeException::class);
        static::expectExceptionMessage('test-message');
        $facade->doRequest($params);
    }

    private function buildService(): MapResourceInteractionFacade
    {
        return  new MapResourceInteractionFacade(
            $this->httpClient,
            $this->builder,
            $this->parser
        );
    }
}
