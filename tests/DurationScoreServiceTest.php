<?php

namespace RL\Tests;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RL\DurationScoreService;
use RL\Model\TravelParamsDTO;
use RL\Strategy\TimeEstimationStrategyInterface;
use RL\Transformer\DurationScoreTransformerInterface;

final class DurationScoreServiceTest extends TestCase
{
    /** @var MockObject|TimeEstimationStrategyInterface */
    private $durationStrategy;
    /** @var MockObject|DurationScoreTransformerInterface */
    private $scoreTransformer;

    public function setUp(): void
    {
        $this->durationStrategy = $this->getMockBuilder(TimeEstimationStrategyInterface::class)->getMock();
        $this->scoreTransformer = $this->getMockBuilder(DurationScoreTransformerInterface::class)->getMock();
    }

    public function testGetScoreMethodFlow(): void
    {
        $params = (new TravelParamsDTO())
            ->setLatitudeFrom(52.52)
            ->setLongitudeFrom(13.40)
            ->setLatitudeTo(48.13)
            ->setLongitudeTo(11.58);

        $testDuration = 333;
        $testScore = 999;

        $service = $this->buildService();

        $this->durationStrategy->expects($this->once())->method('estimate')
            ->with($params)
            ->willReturn($testDuration);

        $this->scoreTransformer->expects($this->once())->method('transform')
            ->with($testDuration)
            ->willReturn($testScore);

        static::assertEquals($testScore, $service->getScore($params));
    }

    private function buildService(): DurationScoreService
    {
        return new DurationScoreService(
            $this->durationStrategy,
            $this->scoreTransformer
        );
    }
}
